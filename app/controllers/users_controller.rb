class UsersController < ApplicationController
  before_action :logged_in_user, only: [:index, :edit, :update, :destroy]

  def index
    @user = User.find(params[:id])
    if params[:search]
      @todos = @user.todos.where(:date => Date.parse(params[:search]))
    else
      @todos = @user.todos.paginate(page: params[:page])
    end
      # @support_histories = current_agent.support_histories.where(:created_at => Date.parse(params[:selected_date]))
  end

  def edit
    @todo = current_user.todos.build if logged_in?
    @user = User.find(params[:id])
  end


  def show
    @user = User.find(params[:id])
    @todos = @user.todos.paginate(page: params[:page])

  end

  def new
  	@user = User.new
  end

  def create
    @user = User.new(user_params)    # Not the final implementation!
    if @user.save
      log_in @user
      flash[:success] = "Welcome to the Todo List!"
      redirect_to @user
      # Handle a successful save.
    else
      render 'new'
    end
  end

  def search
    @user = User.find(params[:id])
    # @todos = @user.todos.paginate(page: params[:page])
  end


  private

    def user_params
      params.require(:user).permit(:name, :email, :password,
                                   :password_confirmation)
    end

    def correct_user
    	@user = User.find(params[:id])
    	redirect_to(root_url) unless correct_user?(@user)
    end
end
