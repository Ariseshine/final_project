class TodosController < ApplicationController
  before_action :logged_in_user

  # GET /todos
  # GET /todos.json
  def index
    if params[:search]
      @todos = Todo.where(:date => Date.parse(params[:search]))
    else
      @todos = Todo.all
    end
      # @support_histories = current_agent.support_histories.where(:created_at => Date.parse(params[:selected_date]))
  end

  # GET /todo/1
  # GET /todos/1.json
  def show
  end

  # GET /todos/new
  def new
    @todo = Todo.new
  end

  # GET /todos/1/edit
  def edit
  end

  # POST /todos
  # POST /todos.json
  def create
    @todo = current_user.todos.build(todo_params)

    if @todo.save
      flash[:success] = "Todo created!"
      redirect_to root_url
    else
      render 'static_pages/home'
    end
  end


    
  def update
    respond_to do |format|
      if @todo.update(todo_params)
        format.html { redirect_to @todo, notice: 'Todo list was successfully updated.' }
        format.json { render :show, status: :ok, location: @todo }
      else
        format.html { render :edit }
        format.json { render json: @todo.errors, status: :unprocessable_entity }
      end
    end
  end

  def done
    set_todo
    @todo.update_attribute(:status, "Done")
    redirect_to current_user
    # redirect_to @todo, notice: "Todo item completed"
  end

  def undo
    set_todo
    @todo.update_attribute(:status, "Created")
    redirect_to current_user
  end

  # DELETE /todos/1
  # DELETE /todos/1.json
  def destroy
    set_todo
    @todo.destroy
    flash[:success] = "Todo delete"
    respond_to do |format|
      format.html { redirect_to current_user, notice: 'Todo list was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_todo
      @todo = Todo.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def todo_params
      params.require(:todo).permit(:date, :due_time, :description, :status)
    end
end

