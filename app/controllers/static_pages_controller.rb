class StaticPagesController < ApplicationController
  def home
  	@todo = current_user.todos.build if logged_in?
  end

  def help
  end

  def about
  end

  def contact
  end

end
