class Todo < ApplicationRecord
  belongs_to :user
  default_scope -> { order(date: :desc) }
  validates :user_id, presence: true
  validates :description, presence: { message: "must be given please" }
  validates :date, presence: true
  validates :due_time, presence: true
  validates :status, presence: true

  
  def done?
  	if status == "Done" then
  		return true
  	else
  		return false
  	end
  end
end
