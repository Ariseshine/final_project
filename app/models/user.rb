class User < ApplicationRecord
	has_many :todos, dependent: :destroy
	before_save { self.email = email.downcase }
	validates :name, presence: true
	VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
	validates :email, presence: true,
                    format: { with: VALID_EMAIL_REGEX },
                    uniqueness: { case_sensitive: false }
  has_secure_password
  VALID_PASSWORD_REGEX = /\A(?=.*[a-z])(?=.*\d)[a-z\d]{6,}\Z/
  validates :password, presence: true,
  										format: { with: VALID_PASSWORD_REGEX }

  def feed
  	Todo.where("user_id = ?", id)
  end

  
end
