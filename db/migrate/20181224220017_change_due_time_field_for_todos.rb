class ChangeDueTimeFieldForTodos < ActiveRecord::Migration[5.2]
  def change
  	change_column :todos, :due_time, :time
  end
end
