Rails.application.routes.draw do

  root to: 'static_pages#home'
  get 'static_pages/help'
  get 'sessions/new'
	get    '/login',   to: 'sessions#new'
  post   '/login',   to: 'sessions#create'
  delete '/logout',  to: 'sessions#destroy'
	get    '/signup',  to: 'users#new'
  post   '/signup',  to: 'users#create'
  resources :users
  resources :todos do
    member do
        patch :done
        patch :undo
    end
  end
  get '/search' => 'user#search', :as => 'search_user'
  resources :sessions,      only: [:new, :create, :destroy]
  match '/signup',  to: 'users#new',            via: 'get'
  match '/signin',  to: 'sessions#new',         via: 'get'
  match '/signout', to: 'sessions#destroy',     via: 'delete'
  match '/help',    to: 'static_pages#help',    via: 'get'
  match '/about',   to: 'static_pages#about',   via: 'get'
  match '/contact', to: 'static_pages#contact', via: 'get'
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
end
