require 'rails_helper'

feature "Visit homepage" do 

	scenario "Successfully" do
		visit root_path
		expect(page).to have_content "Sign up now!"
		click_on 'Sign up now!'
		fill_in 'Name', with: 'Ruby'
		fill_in 'Email', with: 'ruby@ruby.com'
		fill_in 'Password', with: 'ruby123'
		fill_in 'Confirmation', with: 'ruby123'
		click_on 'Create my account'

		expect(page).to have_content('Welcome to the Todo List!')
	end

	scenario "No Name" do
		visit root_path
		expect(page).to have_content "Sign up now!"
		click_on 'Sign up now!'
		fill_in 'Name', with: ''
		fill_in 'Email', with: 'ruby@ruby.com'
		fill_in 'Password', with: 'ruby123'
		fill_in 'Confirmation', with: 'ruby123'
		click_on 'Create my account'

		expect(page).to have_content("Name can't be blank")
	end

	scenario "No Email" do
		visit root_path
		expect(page).to have_content "Sign up now!"
		click_on 'Sign up now!'
		fill_in 'Name', with: 'Ruby'
		fill_in 'Email', with: ''
		fill_in 'Password', with: 'ruby123'
		fill_in 'Confirmation', with: 'ruby123'
		click_on 'Create my account'

		expect(page).to have_content("Email can't be blank")
	end
	scenario "Invalid Password" do
		visit root_path
		expect(page).to have_content "Sign up now!"
		click_on 'Sign up now!'
		fill_in 'Name', with: 'Ruby'
		fill_in 'Email', with: 'ruby@ruby.com'
		fill_in 'Password', with: 'ruby'
		fill_in 'Confirmation', with: 'ruby'
		click_on 'Create my account'

		expect(page).to have_content("Password is invalid")
	end

	scenario "Invalid Password" do
		visit root_path
		expect(page).to have_content "Sign up now!"
		click_on 'Sign up now!'
		fill_in 'Name', with: 'Ruby'
		fill_in 'Email', with: 'ruby@ruby.com'
		fill_in 'Password', with: 'rubyruby'
		fill_in 'Confirmation', with: 'rubyruby'
		click_on 'Create my account'

		expect(page).to have_content("Password is invalid")
	end
	scenario "Invalid Password" do
		visit root_path
		expect(page).to have_content "Sign up now!"
		click_on 'Sign up now!'
		fill_in 'Name', with: 'Ruby'
		fill_in 'Email', with: 'ruby@ruby.com'
		fill_in 'Password', with: '123456'
		fill_in 'Confirmation', with: '123456'
		click_on 'Create my account'

		expect(page).to have_content("Password is invalid")
	end
end