require 'rails_helper'

feature "Delete post", driver: :selenium_chrome_headless do 
	
	scenario "Should delete " do
		user = User.create!(name: "Ruby", email: "ruby@ruby.com", password: "ruby123")
		visit root_path
		expect(page).to have_content "Sign in"
		
		visit root_path
		expect(page).to have_content "Sign in"
		click_on 'Sign in'
		
		fill_in 'Email', with: 'ruby@ruby.com'
		fill_in 'Password', with: 'ruby123'
		
		click_on 'Log in'
		
		expect(page).to have_content('Ruby')
		expect(page).to have_content('Sign out')
		expect(page).to have_content "New Todo List"
		click_on "New Todo List"
		expect(page).to have_content "Description"
		fill_in "Description", with: 'Ruby'
		fill_in "Date", with: '12/12/2020'
		fill_in "Due time", with: '12:12AM'
		find(:xpath,'//*[@id="todo_status_created"]').click
		click_on "Post"
		click_on "view my profile"
		click_on "delete"
		expect(page).to have_content('Todo list was successfully destroyed.')
	end
end