require 'rails_helper'

feature "Visit homepage" do 

	scenario "Sign in and Sign out Successfully" do
		visit root_path
		
		click_on 'Sign up now!'
		fill_in 'Name', with: 'Ruby'
		fill_in 'Email', with: 'ruby@ruby.com'
		fill_in 'Password', with: 'ruby123'
		fill_in 'Confirmation', with: 'ruby123'
		click_on 'Create my account'
		click_on 'Sign out'


		
		visit root_path
		expect(page).to have_content "Sign in"
		click_on 'Sign in'
		
		fill_in 'Email', with: 'ruby@ruby.com'
		fill_in 'Password', with: 'ruby123'
		
		click_on 'Log in'
		
		expect(page).to have_content('Ruby')
		expect(page).to have_content('Sign out')
		click_on 'Sign out'
		expect(page).to have_content('Log out successfully')
	end
end
