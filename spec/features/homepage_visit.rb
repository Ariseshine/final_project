require 'rails_helper'

feature "Visit homepage" do 

	scenario "Successfully" do
		visit root_path
		expect(page).to have_content "Sign up now!"
	end
end